#!/bin/sh
(cd ../../po/ && msgcat --lang=fr --use-first fr.po fr.po.staging -o fr.po)
(cd ../../qt5-dev/po/ && make update-po)
# 1) stash other than fr.po fr.gmo
# 2) check with poedit and
#    git diff remotes/github/staging fr.po | grep "^[^ @][^#]"
