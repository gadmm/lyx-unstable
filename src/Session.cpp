/**
 * \file Session.cpp
 * This file is part of LyX, the document processor.
 * Licence details can be found in the file COPYING.
 *
 * \author Lars Gullik Bjønnes
 * \author Bo Peng
 *
 * Full author contact details are available in file CREDITS.
 */

#include <config.h>

#include "Session.h"

#include "frontends/Application.h"

#include "support/debug.h"
#include "support/filetools.h"
#include "support/Package.h"

#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;
using namespace lyx::support;

namespace {

string const sec_lastfiles = "[recent files]";
string const sec_lastfilepos = "[cursor positions]";
string const sec_lastopened = "[last opened files]";
string const sec_bookmarks = "[bookmarks]";
string const sec_session = "[session info]";
string const sec_toolbars = "[toolbars]";
string const sec_lastcommands = "[last commands]";
string const sec_authfiles = "[auth files]";

} // namespace


namespace lyx {

LastFilesSection::LastFilesSection(unsigned int num) :
	default_num_last_files(4),
	absolute_max_last_files(100)
{
	setNumberOfLastFiles(num);
}


void LastFilesSection::read(istream & is)
{
	string tmp;
	do {
		char c = is.peek();
		if (c == '[')
			break;
		getline(is, tmp);
		if (tmp.empty() || tmp[0] == '#' || tmp[0] == ' ' || !FileName::isAbsolute(tmp))
			continue;

		// read lastfiles
		FileName const file(tmp);
		if (file.exists() && !file.isDirectory()
		    && lastfiles.size() < num_lastfiles)
			lastfiles.push_back(file);
		else
			LYXERR(Debug::INIT, "LyX: Warning: Ignore last file: " << tmp);
	} while (is.good());
}


void LastFilesSection::write(ostream & os) const
{
	os << '\n' << sec_lastfiles << '\n';
	copy(lastfiles.begin(), lastfiles.end(),
	     ostream_iterator<FileName>(os, "\n"));
}


void LastFilesSection::add(FileName const & file)
{
	// If file already exist, delete it and reinsert at front.
	LastFiles::iterator it = find(lastfiles.begin(), lastfiles.end(), file);
	if (it != lastfiles.end())
		lastfiles.erase(it);
	lastfiles.insert(lastfiles.begin(), file);
	if (lastfiles.size() > num_lastfiles)
		lastfiles.pop_back();
}


void LastFilesSection::setNumberOfLastFiles(unsigned int no)
{
	if (0 < no && no <= absolute_max_last_files)
		num_lastfiles = no;
	else {
		LYXERR(Debug::INIT, "LyX: session: too many last files\n"
			<< "\tdefault (=" << default_num_last_files << ") used.");
		num_lastfiles = default_num_last_files;
	}
}


namespace {

// Parse and write the workarea location. The format is forward-compatible for
// when the view is extended with nested QSplitters (at the cost of somu
// redudancy in the information currently).
// e.g. h3v2 -> 3rd horizontal, 2nd vertical
// Currently LyX only handles h3, v3...

void writeWorkareaLocation(ostream & os,
                           LastOpenedSection::LastOpenedFile const & lof)
{
	os << (lof.horizontal ? "h" : "v") << lof.column;
}


LastOpenedSection::LastOpenedFile readWorkareaLocation(istream & is)
{
	LastOpenedSection::LastOpenedFile lof;
	char h = is.get();
	lof.horizontal = h == 'h';
	is >> lof.column;
	return lof;
}

} // namespace


void LastOpenedSection::read(istream & is)
{
	string tmp;
	// old format (backwards compat): (active:bool), (filename:string)
	// new format: (column&horizontal:location), (view:bool), (active:bool), (filename:string)
	do {
		char c = is.peek();
		if (c == '[')
			break;
		getline(is, tmp);
		if (tmp.empty() || tmp[0] == '#' || tmp[0] == ' ')
			continue;

		try {
			LastOpenedFile lof;
			istringstream itmp(tmp);
			// skip to parsing lof.active for backwards-compatibility
			if (tmp[0] == 'h' || tmp[0] == 'v') {
				lof = readWorkareaLocation(itmp);
				itmp.ignore(2);  // ignore ", "
				itmp >> lof.view;
				itmp.ignore(2);
			}
			itmp >> lof.active;
			itmp.ignore(2);
			string fname;
			getline(itmp, fname);
			if (!FileName::isAbsolute(fname))
				continue;

			FileName const file(fname);
			if (file.exists() && !file.isDirectory()) {
				lof.file_name = file;
				lastopened_.push_back(lof);
			} else {
				LYXERR(Debug::INIT,
					"LyX: Warning: Ignore last opened file: " << tmp);
			}
		} catch (...) {
			LYXERR(Debug::INIT,
				"LyX: Warning: unknown state of last opened file: " << tmp);
		}
	} while (is.good());
}


void LastOpenedSection::write(ostream & os) const
{
	os << '\n' << sec_lastopened << '\n';
	for (size_t i = 0; i < lastopened_.size(); ++i) {
		writeWorkareaLocation(os, lastopened_[i]);
		os << ", " << lastopened_[i].view << ", "
		   << lastopened_[i].active << ", "
		   << lastopened_[i].file_name << '\n';
	}
}


void LastOpenedSection::add(FileName const & file,
                            bool active,
							int view,
                            int column,
                            bool horizontal)
{
	lastopened_.push_back({ .file_name = file,
				.active = active,
				.view = view,
				.column = column,
				.horizontal = horizontal
				});
}


void LastOpenedSection::clearView(int view)
{
	// put matching elements at the end
	std::stable_sort(lastopened_.begin(), lastopened_.end(),
					 [view](LastOpenedFile const & a, LastOpenedFile const & b) {
						 return a.view != view && b.view == view;
					 });
	// remove the end
	auto first_in_view = std::find_if(lastopened_.begin(), lastopened_.end(),
									  [view](LastOpenedFile const & a) {
										  return a.view == view;
									  });
	lastopened_.erase(first_in_view, lastopened_.end());
}


void LastOpenedSection::clear()
{
	lastopened_.clear();
}


unsigned int const LastFilePosSection::num_last_file_pos_ = 100;


void LastFilePosSection::read(istream & is)
{
	string tmp;
	do {
		char c = is.peek();
		if (c == '[')
			break;
		getline(is, tmp);
		if (tmp == "" || tmp[0] == '#' || tmp[0] == ' ')
			continue;

		try {
			// read lastfilepos
			// pos, file\n
			FilePos filepos;
			string fname;
			istringstream itmp(tmp);
			itmp >> filepos.pit;
			itmp.ignore(2);  // ignore ", "
			itmp >> filepos.pos;
			itmp.ignore(2);  // ignore ", "
			getline(itmp, fname);
			if (!FileName::isAbsolute(fname))
				continue;
			FileName const file(fname);
			if (file.exists() && !file.isDirectory()
			    && last_file_pos_.size() < num_last_file_pos_)
				last_file_pos_.emplace_back(file, filepos);
			else
				LYXERR(Debug::INIT, "LyX: Warning: Ignore pos of last file: " << fname);
		} catch (...) {
			LYXERR(Debug::INIT, "LyX: Warning: unknown pos of last file: " << tmp);
		}
	} while (is.good());
}


void LastFilePosSection::write(ostream & os) const
{
	os << '\n' << sec_lastfilepos << '\n';
	for (pair<FileName, FilePos> const & p : last_file_pos_)
		os << p.second.pit << ", " << p.second.pos << ", "
		   << p.first << '\n';
}


void LastFilePosSection::save(FileName const & fname, FilePos pos)
{
	last_file_pos_.remove_if([&fname](pair<FileName, FilePos> const & p){
			return fname == p.first;
		});
	last_file_pos_.emplace_front(fname, pos);
}


LastFilePosSection::FilePos LastFilePosSection::load(FileName const & fname) const
{
	for (pair<FileName, FilePos> const & p : last_file_pos_)
		if (p.first == fname)
			return p.second;
	return {};
}


void BookmarksSection::clear()
{
	// keep bookmark[0], the temporary one
	bookmarks.resize(1);
	bookmarks.resize(max_bookmarks + 1);
}


void BookmarksSection::read(istream & is)
{
	string tmp;
	do {
		char c = is.peek();
		if (c == '[')
			break;
		getline(is, tmp);
		if (tmp == "" || tmp[0] == '#' || tmp[0] == ' ')
			continue;

		try {
			// read bookmarks
			// idx, pit, pos, file\n
			unsigned int idx;
			pit_type pit;
			pos_type pos;
			string fname;
			istringstream itmp(tmp);
			itmp >> idx;
			itmp.ignore(2);  // ignore ", "
			itmp >> pit;
			itmp.ignore(2);  // ignore ", "
			itmp >> pos;
			itmp.ignore(2);  // ignore ", "
			getline(itmp, fname);
			if (!FileName::isAbsolute(fname))
				continue;
			FileName const file(fname);
			// only load valid bookmarks
			if (file.exists() && !file.isDirectory() && idx <= max_bookmarks)
				bookmarks[idx] = Bookmark(file, pit, pos, 0, 0);
			else
				LYXERR(Debug::INIT, "LyX: Warning: Ignore bookmark of file: " << fname);
		} catch (...) {
			LYXERR(Debug::INIT, "LyX: Warning: unknown Bookmark info: " << tmp);
		}
	} while (is.good());
}


void BookmarksSection::write(ostream & os) const
{
	os << '\n' << sec_bookmarks << '\n';
	for (size_t i = 0; i <= max_bookmarks; ++i) {
		if (isValid(i))
			os << i << ", "
			   << bookmarks[i].bottom_pit << ", "
			   << bookmarks[i].bottom_pos << ", "
			   << bookmarks[i].filename << '\n';
	}
}


void BookmarksSection::save(FileName const & fname,
	pit_type bottom_pit, pos_type bottom_pos,
	int top_id, pos_type top_pos, unsigned int idx)
{
	// silently ignore bookmarks when idx is out of range
	if (idx <= max_bookmarks)
		bookmarks[idx] = Bookmark(fname, bottom_pit, bottom_pos, top_id, top_pos);
}


bool BookmarksSection::isValid(unsigned int i) const
{
	return i <= max_bookmarks && !bookmarks[i].filename.empty();
}


bool BookmarksSection::hasValid() const
{
	for (size_t i = 1; i <= size(); ++i) {
		if (isValid(i))
			return true;
	}
	return false;
}


BookmarksSection::Bookmark const & BookmarksSection::bookmark(unsigned int i) const
{
	return bookmarks[i];
}


LastCommandsSection::LastCommandsSection(unsigned int num) :
	default_num_last_commands(30),
	absolute_max_last_commands(100)
{
	setNumberOfLastCommands(num);
}


void LastCommandsSection::read(istream & is)
{
	string tmp;
	do {
		char c = is.peek();
		if (c == '[')
			break;
		getline(is, tmp);
		if (tmp == "" || tmp[0] == '#' || tmp[0] == ' ')
			continue;

		lastcommands.push_back(tmp);
	} while (is.good());
}


void LastCommandsSection::write(ostream & os) const
{
	os << '\n' << sec_lastcommands << '\n';
	copy(lastcommands.begin(), lastcommands.end(),
		ostream_iterator<std::string>(os, "\n"));
}


void LastCommandsSection::setNumberOfLastCommands(unsigned int no)
{
	if (0 < no && no <= absolute_max_last_commands)
		num_lastcommands = no;
	else {
		LYXERR(Debug::INIT, "LyX: session: too many last commands\n"
			<< "\tdefault (=" << default_num_last_commands << ") used.");
		num_lastcommands = default_num_last_commands;
	}
}


void LastCommandsSection::add(std::string const & string)
{
	lastcommands.push_back(string);
}


void LastCommandsSection::clear()
{
	lastcommands.clear();
}


Session::Session(unsigned int num_last_files, unsigned int num_last_commands) :
	last_files(num_last_files), last_commands(num_last_commands)
{
	// locate the session file
	// note that the session file name 'session' is hard-coded
	session_file = FileName(addName(package().user_support().absFileName(), "session"));
	//
	readFile();
}


void Session::readFile()
{
	// we will not complain if we can't find session_file nor will
	// we issue a warning. (Lgb)
	ifstream is(session_file.toFilesystemEncoding().c_str());
	string tmp;

	while (getline(is, tmp)) {
		// Ignore comments, empty line or line stats with ' '
		if (tmp == "" || tmp[0] == '#' || tmp[0] == ' ')
			continue;

		// Determine section id
		if (tmp == sec_lastfiles)
			lastFiles().read(is);
		else if (tmp == sec_lastopened)
			lastOpened().read(is);
		else if (tmp == sec_lastfilepos)
			lastFilePos().read(is);
		else if (tmp == sec_bookmarks)
			bookmarks().read(is);
		else if (tmp == sec_lastcommands)
			lastCommands().read(is);
		else if (tmp == sec_authfiles)
			authFiles().read(is);

		else
			LYXERR(Debug::INIT, "LyX: Warning: unknown Session section: " << tmp);
	}
}


void Session::writeFile() const
{
	if (theApp())
		theApp()->writeGuiSession();
	ofstream os(session_file.toFilesystemEncoding().c_str());
	if (os) {
		os << "## Automatically generated lyx session file \n"
		    << "## Editing this file manually may cause lyx to crash.\n";

		lastFiles().write(os);
		lastOpened().write(os);
		lastFilePos().write(os);
		lastCommands().write(os);
		bookmarks().write(os);
		authFiles().write(os);
	} else
		LYXERR(Debug::INIT, "LyX: Warning: unable to save Session: "
		       << session_file);
}


void AuthFilesSection::read(istream & is)
{
	string tmp;
	do {
		char c = is.peek();
		if (c == '[')
			break;
		getline(is, tmp);
		if (tmp.empty() || tmp[0] == '#' || tmp[0] == ' ' || !FileName::isAbsolute(tmp))
			continue;

		// read lastfiles
		FileName const file(tmp);
		if (file.exists() && !file.isDirectory())
			auth_files_.insert(tmp);
		else
			LYXERR(Debug::INIT, "LyX: Warning: Ignore auth file: " << tmp);
	} while (is.good());
}


void AuthFilesSection::write(ostream & os) const
{
	os << '\n' << sec_authfiles << '\n';
	copy(auth_files_.begin(), auth_files_.end(),
	     ostream_iterator<std::string>(os, "\n"));
}


bool AuthFilesSection::find(string const & name) const
{
	if (auth_files_.find(name) != auth_files_.end())
		return true;

	return false;
}


void AuthFilesSection::insert(string const & name)
{
	auth_files_.insert(name);
}


void AuthFilesSection::remove(string const & name)
{
	set<string>::iterator it = auth_files_.find(name);
	if (it != auth_files_.end())
		auth_files_.erase(it);
}


} // namespace lyx
