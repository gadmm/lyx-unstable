/**
 * \file FileMonitor.cpp
 * This file is part of LyX, the document processor.
 * Licence details can be found in the file COPYING.
 *
 * \author Angus Leeming
 * \author Guillaume Munch
 *
 * Full author contact details are available in file CREDITS.
 */

#include <config.h>

#include "support/FileMonitor.h"

#include "support/debug.h"
#include "support/FileName.h"
#include "support/qstring_helpers.h"
#include "support/unique_ptr.h"

#include <QFile>
#include <QStringList>
#include <QTimer>

#include <algorithm>

using namespace std;

namespace lyx {
namespace support {


FileSystemWatcher & FileSystemWatcher::instance()
{
	thread_local FileSystemWatcher f;
	return f;
}


FileSystemWatcher::FileSystemWatcher()
	: qwatcher_(make_unique<QFileSystemWatcher>())
{}


shared_ptr<FileWatcher>
FileSystemWatcher::getGuard(FileName const & filename)
{
	string const absfilename = filename.absFileName();
	weak_ptr<FileWatcher> & wptr = store_[absfilename];
	if (shared_ptr<FileWatcher> mon = wptr.lock())
		return mon;
	auto mon = make_shared<FileWatcher>(absfilename, qwatcher_.get());
	wptr = mon;
	return mon;
}


//static
FileMonitorPtr FileSystemWatcher::monitor(FileName const & filename)
{
	return make_unique<FileMonitor>(instance().getGuard(filename));
}


//static
ActiveFileMonitorPtr FileSystemWatcher::activeMonitor(FileName const & filename,
                                                      int interval)
{
	return make_unique<ActiveFileMonitor>(instance().getGuard(filename),
	                                      filename, interval);
}


//static
void FileSystemWatcher::debug()
{
	FileSystemWatcher & f = instance();
	QStringList q_files = f.qwatcher_->files();
	for (pair<string, weak_ptr<FileWatcher>> pair : f.store_) {
		string const & name = pair.first;
		if (!pair.second.expired()) {
			if (!q_files.contains(toqstr(name)))
				LYXERR0("Monitored but not QFileSystemWatched (bad): " << name);
			else {
				//LYXERR0("Monitored and QFileSystemWatched (good): " << name);
			}
		}
	}
	for (QString const & qname : q_files) {
		string const name = fromqstr(qname);
		weak_ptr<FileWatcher> & wptr = f.store_[name];
		if (wptr.expired())
			LYXERR0("QFileSystemWatched but not monitored (bad): " << name);
	}
}


FileWatcher::FileWatcher(string const & filename,
                                   QFileSystemWatcher * qwatcher)
	: filename_(filename), qwatcher_(qwatcher), exists_(true)
{
	if (filename.empty())
		return;
	QObject::connect(qwatcher, &QFileSystemWatcher::fileChanged,
	                 this, &FileWatcher::notifyChange);
	if (qwatcher_->files().contains(toqstr(filename)))
		LYXERR0("This file is already being QFileSystemWatched: " << filename
		        << ". This should not happen, please report.");
	refresh(true);
}


FileWatcher::~FileWatcher()
{
	if (!filename_.empty())
		qwatcher_->removePath(toqstr(filename_));
}


void FileWatcher::refresh(bool const emit)
{
	if (filename_.empty())
		return;
	QString const qfilename = toqstr(filename_);
	if (!qwatcher_->files().contains(qfilename)) {
		bool const existed = exists_;
		exists_ = QFile(qfilename).exists();
		if (exists_ && !qwatcher_->addPath(qfilename)) {
			LYXERR(Debug::FILES,
			       "Could not add path to QFileSystemWatcher: " << filename_);
			QTimer::singleShot(5000, this, [=](){ refresh(true); });
		} else {
			if (!exists_)
				// The standard way to overwrite a file is to delete it and
				// create a new file with the same name. Therefore if the file
				// has just been deleted, it is smart to check not too long
				// after whether it has been recreated.
			    QTimer::singleShot(existed ? 100 : 2000, this,
			                       [=](){ refresh(true); });
			if (existed != exists_ && emit)
				Q_EMIT fileChanged(exists_);
		}
	}
}


void FileWatcher::notifyChange(QString const & path)
{
	if (path == toqstr(filename_)) {
		// If the file has been modified by delete-move, we are notified of the
		// deletion but we no longer track the file. See
		// <https://bugreports.qt.io/browse/QTBUG-46483> (not a bug).
		// Therefore we must refresh.
		refresh(false);
		Q_EMIT fileChanged(exists_);
	}
}


FileMonitor::FileMonitor(std::shared_ptr<FileWatcher> watcher)
	: watcher_(watcher)
{
	// Connections need to be asynchronous because the receiver can own this
	// object and therefore is allowed to delete it.
	// Qt signal:
	QObject::connect(watcher_.get(), &FileWatcher::fileChanged,
	                 this, &FileMonitor::fileChanged,
	                 Qt::QueuedConnection);
	// Boost signal:
	QObject::connect(this, &FileMonitor::fileChanged,
	                 this, [=](bool exists) { fileChanged_(exists); },
	                 Qt::QueuedConnection);
	refresh();
}


signals2::connection FileMonitor::connect(slot const & slot)
{
	return fileChanged_.connect(slot);
}


ActiveFileMonitor::ActiveFileMonitor(std::shared_ptr<FileWatcher> watcher,
                                     FileName const & filename, int interval)
	: FileMonitor(watcher), filename_(filename), interval_(interval),
	  timestamp_(0), checksum_(0), cooldown_(true)
{
	QObject::connect(this, &FileMonitor::fileChanged,
	                 this, &ActiveFileMonitor::setCooldown);
	QTimer::singleShot(interval_, this,
	                   &ActiveFileMonitor::clearCooldown);
	filename_.refresh();
	if (!filename_.exists())
		return;
	timestamp_ = filename_.lastModified();
	checksum_ = filename_.checksum();
}


void ActiveFileMonitor::checkModified()
{
	if (cooldown_)
		return;

	cooldown_ = true;
	bool changed = false;
	filename_.refresh();
	bool exists = filename_.exists();
	if (!exists) {
		changed = timestamp_ || checksum_;
		timestamp_ = 0;
		checksum_ = 0;
	} else {
		time_t const new_timestamp = filename_.lastModified();

		if (new_timestamp != timestamp_) {
			timestamp_ = new_timestamp;

			unsigned long const new_checksum = filename_.checksum();
			if (new_checksum != checksum_) {
				checksum_ = new_checksum;
				changed = true;
			}
		}
	}
	if (changed)
		Q_EMIT FileMonitor::fileChanged(exists);
	QTimer::singleShot(interval_, this, &ActiveFileMonitor::clearCooldown);
}


void ActiveFileMonitor::checkModifiedAsync()
{
	if (!cooldown_)
		QTimer::singleShot(0, this, SLOT(checkModified()));
}



} // namespace support
} // namespace lyx

#include "moc_FileMonitor.cpp"
